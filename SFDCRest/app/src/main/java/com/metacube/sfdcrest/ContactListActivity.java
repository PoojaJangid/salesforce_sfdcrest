package com.metacube.sfdcrest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.salesforce.androidsdk.app.SalesforceSDKManager;
import com.salesforce.androidsdk.rest.ClientManager;
import com.salesforce.androidsdk.rest.RestClient;
import com.salesforce.androidsdk.rest.RestRequest;
import com.salesforce.androidsdk.rest.RestResponse;

import org.json.JSONArray;

import java.io.UnsupportedEncodingException;

public class ContactListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);
        try {
            onFetchContactsClick();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }


    public void onFetchContactsClick() throws UnsupportedEncodingException {

        sendRequest("SELECT Name,Id from Contact Order by Name");

    }


    private void sendRequest(String soql) throws UnsupportedEncodingException
    {
        final RestRequest restRequest =
                RestRequest.getRequestForQuery(
                        getString(R.string.api_version), soql);
        SalesforceSDKManager.getInstance().getClientManager().getRestClient(this, new ClientManager.RestClientCallback() {
            @Override
            public void authenticatedRestClient(RestClient client) {
                client.sendAsync(restRequest, new RestClient.AsyncRequestCallback()
                {
                    @Override
                    public void onSuccess(RestRequest request,
                                          RestResponse result) {
                        try {
                           // listAdapter.clear();
                            JSONArray records =
                                    result.asJSONObject().getJSONArray("records");
                            for (int i = 0; i < records.length(); i++) {
//                                listAdapter.add(
//                                        records.getJSONObject(i).getString("Name"));
                            }
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                    @Override
                    public void onError(Exception exception)
                    {
                        Toast.makeText(ContactListActivity.this,
                                ContactListActivity.this.getString(
                                        SalesforceSDKManager.getInstance().
                                                getSalesforceR().stringGenericError(),
                                        exception.toString()),
                                Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

    }

}
