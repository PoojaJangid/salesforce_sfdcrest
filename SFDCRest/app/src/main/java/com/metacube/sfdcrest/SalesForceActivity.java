package com.metacube.sfdcrest;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.salesforce.androidsdk.app.SalesforceSDKManager;
import com.salesforce.androidsdk.rest.ClientManager;
import com.salesforce.androidsdk.rest.RestClient;
import com.salesforce.androidsdk.ui.SalesforceActivity;

import java.util.List;


public class SalesForceActivity extends SalesforceActivity {


    @Override
    public void onResume(RestClient client) {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_force);




        String accountType =
                SalesforceSDKManager.getInstance().getAccountType();

        ClientManager.LoginOptions loginOptions =
                SalesforceSDKManager.getInstance().getLoginOptions();
        // Get a rest client
        new ClientManager(this, accountType, loginOptions,
                SalesforceSDKManager.getInstance().
                        shouldLogoutWhenTokenRevoked()).
                getRestClient(this, new ClientManager.RestClientCallback() {
                            @Override
                            public void
                            authenticatedRestClient(RestClient client) {
                                if (client == null) {
                                    SalesforceSDKManager.getInstance().
                                            logout(SalesForceActivity.this);
                                    Toast.makeText(SalesForceActivity.this, getResources()
                                            .getString(R.string.salesforce_error), Toast
                                            .LENGTH_LONG).show();
                                    return;
                                } else {
                                    Intent intent = new Intent(SalesForceActivity.this,
                                            ContactListActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        }
                );
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }




}
